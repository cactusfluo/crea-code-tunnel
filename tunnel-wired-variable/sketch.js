//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	WIDTH				= 800;
const	HEIGHT				= 800;
const	PARALLAX_RATIO		= 1.5;
const	STEP				= 1 + 1 / 100;//1.3;
const	FRAME_CYCLE			= 2;

const	PI					= 3.14159265358979323846 
const	POINTS				= 7;
const	POINT_STEP			= 2 * PI / POINTS;
const	POINT_FRAME_CYCLE	= 100;
const	POINT_FRAME_STEP	= POINT_STEP / POINT_FRAME_CYCLE;

let		g_shapes			= null;
let		g_frame				= 0;
let		g_frame_rotation	= 0;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// P5 MAIN
//////////////////////////////////////////////////

function	setup() {
	let		i;
	let		shape;

	createCanvas(WIDTH, HEIGHT);
	noCursor();
	g_shapes = [];
	mouseX = WIDTH / 2;
	mouseY = HEIGHT / 2;
	/// CREATE ALL SHAPES
	while (true) {
		/// ADD SHAPE
		if (g_frame == 0) {
			g_shapes.push({
				"x": mouseX,
				"y": mouseY,
				"w": 5,
				"particles": []
			});
		}
		/// UPDATE SHAPES
		for (i = 0; i < g_shapes.length; ++i) {
			shape = g_shapes[i];
			if (i < g_shapes.length - 1) {
				shape.x = g_shapes[i + 1].x;
				shape.y = g_shapes[i + 1].y;
			}
			shape.w *= STEP;
		}
		/// QUIT IF SHAPES ALL DONE
		if (g_shapes[0].w > WIDTH * PARALLAX_RATIO) {
			g_shapes.shift();
			break;
		}
		if (++g_frame >= FRAME_CYCLE) {
			g_frame = 0;
		}
	}
	g_frame = 0;
}

function	draw() {
	let		i, j;
	let		x, y;
	let		shape;
	let		parallax;
	let		points, prev_points;

	background(0, 0, 0, 255);
	/// ADD SHAPE
	if (g_frame == 0) {
		g_shapes.push({
			"x": mouseX,
			"y": mouseY,
			"w": 5,
			"particles": []
		});
	}
	/// UPDATE SHAPES
	for (i = 0; i < g_shapes.length; ++i) {
		shape = g_shapes[i];
		/// UPDATE POSITIONS
		if (i < g_shapes.length - 1) {
			shape.x = g_shapes[i + 1].x;
			shape.y = g_shapes[i + 1].y;
		}
		/// UPDATE WIDTH
		shape.w *= STEP;
	}
	/// REMOVE EXITING SHAPES
	while (g_shapes[0].w > WIDTH * PARALLAX_RATIO) {
		g_shapes.shift();
	}
	/// INIT POINTS
	points = [];
	prev_points = [];
	for (i = 0; i < POINTS; ++i) {
		points.push({"x": 0, "y": 0});
		prev_points.push({"x": mouseX, "y": mouseY});
	}
	/// PRINT SHAPES
	for (i = g_shapes.length - 1; i >= 0; --i) {
		shape = g_shapes[i];
		/// COMPUTE VARIABLES
		parallax = max(0, 1 - (shape.w / (WIDTH * PARALLAX_RATIO)));
		x = WIDTH / 2 + ((shape.x - (WIDTH / 2)) * parallax);
		y = HEIGHT / 2 + ((shape.y - (HEIGHT / 2)) * parallax);
		/// PRINT Z LINES
		for (j = 0; j < POINTS; ++j) {
			points[j].x = x + cos(POINT_STEP * j + g_frame_rotation * POINT_FRAME_STEP) * shape.w;
			points[j].y = y + sin(POINT_STEP * j + g_frame_rotation * POINT_FRAME_STEP) * shape.w;
			stroke(g_shapes.length - i);
			line(points[j].x, points[j].y, prev_points[j].x, prev_points[j].y);
			prev_points[j].x = points[j].x;
			prev_points[j].y = points[j].y;
		}
	}
	/// HANDLE ANIMATION FRAME
	if (++g_frame >= FRAME_CYCLE) {
		g_frame = 0;
	}
	if (++g_frame_rotation >= POINT_FRAME_CYCLE) {
		g_frame_rotation = 0;
	}
}
